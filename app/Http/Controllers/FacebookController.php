<?php

namespace App\Http\Controllers;

use App\Library\Services\FootballApi;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\UnauthorizedException;

class FacebookController extends Controller
{
    private $psid;
    private $payload;
    public  $start = 0;

    // Would go for dockerised ENV var for prod instead of const
    public const FACEBOOK_VERIFY_TOKEN          = 'DASDTht34234^%£rfrg';
    public const FACEBOOK_MODE_SUBSCRIBE        = 'subscribe';
    public const FACEBOOK_API_MESSAGE_SEND_URI  = 'https://graph.facebook.com/v2.6/me/messages';
    public const FACEBOOK_API_PROFILE_URI       = 'https://graph.facebook.com/v2.6/me/messenger_profile';

    // Payloads
    public const PAYLOAD_2017_RESULTS   = '2017_results';
    public const PAYLOAD_MENU_INIT      = 'menu_init';
    public const PAYLOAD_LAST_RESULTS   = 'last_results';
    public const PAYLOAD_PSG_RESULTS    = 'psg_results';
    public const PAYLOAD_INIT_BUTTONS   = 'init_button_payload';

    //Region Getters/Setters

    /**
     * Maps payload and routes
     *
     * @return array
     */
    public static function getActionByPayloads(): array
    {
        return [
            self::PAYLOAD_LAST_RESULTS => 'actionLastMatchResults',
            self::PAYLOAD_MENU_INIT    => 'actionInitMenu',
            self::PAYLOAD_PSG_RESULTS  => 'actionPsgResults',
            self::PAYLOAD_INIT_BUTTONS => 'actionInitMenu',
            self::PAYLOAD_2017_RESULTS => 'actionYear2017',
        ];
    }

    private function getAction()
    {
        return self::getActionByPayloads()[$this->payload] ?? 'actionNotFound';
    }
    //Endregion Getters/Setters

    //Region Actions
    /**
     * Returns last matches results
     *
     * @throws Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function actionLastMatchResults(): void
    {
        $apiResults = FootballApi::getLastMatchResults($this->start);
        foreach ($apiResults['records'] as $record) {
            $this->sendMessage(FootballApi::getMatchInfo($record));
        }
        $this->start += 10;
        $this->sendButton('Actions', [
            [
                'type'    => 'postback',
                'title'   => 'Voir plus',
                'payload' => self::PAYLOAD_LAST_RESULTS . '&start=' . $this->start
            ]
        ]);
    }

    /**
     * Returns last Paris SG match results
     *
     * @throws Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function actionPsgResults()
    {
        $apiResults = FootballApi::getPsgResults($this->start);
        foreach ($apiResults['records'] as $record) {
            $this->sendMessage(FootballApi::getMatchInfo($record));
        }
        $this->start += 10;
        $this->sendButton('Actions', [
            [
                'type'    => 'postback',
                'title'   => 'Voir plus',
                'payload' => self::PAYLOAD_PSG_RESULTS . '&start=' . $this->start
            ]
        ]);
    }

    /**
     * @throws Exception
     */
    private function actionNotFound()
    {
        Log::error('Action Not found : ' . $this->payload);
    }

    /**
     * @throws Exception
     */
    private function actionInitStartButton(): void
    {
        if (!$this->psid) {
            throw new Exception('App needs a psid to send a message');
        }
        $client = new Client();
        $options = [
            RequestOptions::JSON => [
                'get_started' => [
                    'payload' => self::PAYLOAD_MENU_INIT,
                ]
            ]
        ];
        $client->post(self::FACEBOOK_API_PROFILE_URI . '?access_token=' . getenv('PAGE_ACCESS_TOKEN'), $options);
    }

    /**
     * Returns match from 2017
     *
     * @throws Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function actionYear2017(): void
    {
        $apiResults = FootballApi::get2017Results($this->start);
        foreach ($apiResults['records'] as $record) {
            $this->sendMessage(FootballApi::getMatchInfo($record));
        }
        $this->start += 10;
        $this->sendButton('Actions', [
            [
                'type'    => 'postback',
                'title'   => 'Voir plus',
                'payload' => self::PAYLOAD_2017_RESULTS . '&start=' . $this->start
            ]
        ]);
    }

    /**
     * @throws Exception
     */
    private function actionInitMenu(): void
    {
        if (!$this->psid) {
            throw new Exception('App needs a psid to send a message');
        }
        $client = new Client();
        $options = [
            RequestOptions::JSON => [
                'persistent_menu' => [
                    [
                        'locale'                  => 'default',
                        'composer_input_disabled' => true,
                        'call_to_actions'         => [
                            [
                                'title'   => 'Voir lezs derniers resultats',
                                'type'    => 'postback',
                                'payload' => self::PAYLOAD_LAST_RESULTS
                            ],
                            [
                                'title'   => 'Voir les resultats du PSG',
                                'type'    => 'postback',
                                'payload' => self::PAYLOAD_PSG_RESULTS
                            ],
                            [
                                'title'   => 'Voir les resultats de 2017',
                                'type'    => 'postback',
                                'payload' => self::PAYLOAD_2017_RESULTS
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $result = $client->post(self::FACEBOOK_API_PROFILE_URI . '?access_token=' . getenv('PAGE_ACCESS_TOKEN'), $options);
    }
    //Endregion Actions

    //Region Public Methods
    /**
     * Entrypoint for the chatbot
     *
     * @param Request $request
     * @return string
     * @throws Exception
     */
    public function webhook(Request $request)
    {
        if ($request->post('object') === 'page') {
            $this->handleSubscription($request);
        }
    }

    /**
     * Used for Facebook callback validation
     *
     * @throws UnauthorizedException
     * @param Request $request
     * @return string
     */
    public function callback(Request $request)
    {
        // Check for mode, verifyToken and send back challenge
        if ($request->get('hub_mode') === self::FACEBOOK_MODE_SUBSCRIBE
            && $request->get('hub_verify_token') === self::FACEBOOK_VERIFY_TOKEN
            && $challenge = $request->get('hub_challenge')) {
            return $challenge;
        }

        throw new UnauthorizedException();
    }
    //Endregion Public Methods

    //Region Private Methods
    /**
     * @param Request $request
     */
    private function handleSubscription(Request $request): void
    {
        foreach ($request->post('entry') as $entry) {
            if (isset($entry['messaging'])) {
                foreach ($entry['messaging'] as $message) {
                    if (isset($message['sender']['id'])) {
                        $this->psid = $message['sender']['id'];
                    }
                    if (isset($message['postback']['payload']) && $payload = $message['postback']['payload']) {
                        $this->payload = $payload;
                        $this->handlePayload();
                    }
                }
            }
        }
    }

    /**
     * @param string $label
     * @param array  $buttons
     * @throws Exception
     */
    private function sendButton(string $label, array $buttons): void
    {
        if (!$this->psid) {
            throw new Exception('App needs a psid to send a message');
        }
        $client = new Client();
        $options = [
            RequestOptions::JSON => [
                'recipient' => [
                    'id' => $this->psid,
                ],
                'message'   => [
                    'attachment' => [
                        'type'    => 'template',
                        'payload' => [
                            'template_type' => 'button',
                            'text'          => $label,
                            'buttons'       => $buttons
                        ]
                    ]
                ],
            ]
        ];
        $client->post(self::FACEBOOK_API_MESSAGE_SEND_URI . '?access_token=' . getenv('PAGE_ACCESS_TOKEN'), $options);
    }

    /**
     * Sends a string message to user
     *
     * @param string $message
     * @throws Exception
     */
    private function sendMessage(string $message): void
    {
        if (!$this->psid) {
            throw new Exception('App needs a psid to send a message');
        }
        $client = new Client();
        $options = [
            RequestOptions::JSON => [
                'recipient' => [
                    'id' => $this->psid,
                ],
                'message'   => ['text' => $message,
                ]
            ]
        ];
        $client->post(self::FACEBOOK_API_MESSAGE_SEND_URI . '?access_token=' . getenv('PAGE_ACCESS_TOKEN'), $options);
    }

    private function handlePayload(): void
    {
        // Enables pagination
        if (strpos($this->payload, '&start=')) {
            $params = explode('&start=', $this->payload);
            $this->payload = $params[0];
            $this->start = $params[1];
        }
        call_user_func([self::class, $this->getAction()]);
    }
    //Endregion Private Methods
}
