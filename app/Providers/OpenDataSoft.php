<?php

namespace App\Providers;

use App\Library\Services\FootballApi;
use Illuminate\Support\ServiceProvider;

class OpenDataSoft extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\DemoOne', function ($app) {
            return new FootballApi();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
