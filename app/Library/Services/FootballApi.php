<?php

namespace App\Library\Services;

use GuzzleHttp\Client;

class FootballApi
{
    public const OPEN_DATA_API_URI  = 'https://public.opendatasoft.com/api/records/1.0/search/?dataset=resultats-ligue-1&';
    public const DEFAULT_ROWS_LIMIT = 10;
    public const DEFAULT_LANGUAGE   = 'FR';
    public const DEFAULT_SORT       = 'start_date';
    public const DEFAULT_START      = 0;
    public const TEAM_PARIS_SG      = 'Paris SG';

    /**
     * Calls OpenDataSoft for 2017 results
     *
     * @param int    $start
     * @param int    $rows
     * @param string $lang
     * @param string $sort
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function get2017Results($start = self::DEFAULT_START, $rows = self::DEFAULT_ROWS_LIMIT, $lang = self::DEFAULT_LANGUAGE, $sort = self::DEFAULT_SORT)
    {
        $client = new Client();
        // Better query handling with http_build_query
        $result = $client->request('GET', self::OPEN_DATA_API_URI . http_build_query(
                [
                    'lang'  => $lang,
                    'rows'  => $rows,
                    'sort'  => $sort,
                    'start' => $start,
                    'q'     => 'start_date=2017'
                ]
            )
        );

        return json_decode($result->getBody(), true);
    }

    /**
     * Calls OpenDataSoft to get last match results
     *
     * @param int    $rows
     * @param string $lang
     * @param string $sort
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getLastMatchResults($start = self::DEFAULT_START, $rows = self::DEFAULT_ROWS_LIMIT, $lang = self::DEFAULT_LANGUAGE, $sort = self::DEFAULT_SORT)
    {
        $client = new Client();
        // Better query handling with http_build_query
        $result = $client->request('GET', self::OPEN_DATA_API_URI . http_build_query(
                [
                    'lang'  => $lang,
                    'rows'  => $rows,
                    'sort'  => $sort,
                    'start' => $start
                ]
            )
        );

        return json_decode($result->getBody(), true);
    }

    /**
     * Returns readable intel from API response
     *
     * @param $match
     * @return string
     */
    public static function getMatchInfo($match): string
    {
        $str = '';

        if (isset($match['fields']) && $fields = $match['fields']) {
            $str .= $fields['start_date'] . ' - ' . $fields['home_team'] . ' ' . $fields['home_goal'] . ' - ' . $fields['away_team'] . ' ' . $fields['away_goal'];
        }

        return $str;
    }

    /**
     * Calls OpenDataSoft to get last PSG matches results
     *
     * @param int    $start
     * @param int    $rows
     * @param string $lang
     * @param string $sort
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getPsgResults($start = self::DEFAULT_START, $rows = self::DEFAULT_ROWS_LIMIT, $lang = self::DEFAULT_LANGUAGE, $sort = self::DEFAULT_SORT)
    {
        $client = new Client();
        $team = self::TEAM_PARIS_SG;
        // Better query handling with http_build_query
        $result = $client->request('GET', self::OPEN_DATA_API_URI . http_build_query(
                ['lang'  => $lang,
                 'rows'  => $rows,
                 'sort'  => $sort,
                 'start' => $start,
                 'q'     => "(home_team={$team} OR away_team={$team})"
                ]
            )
        );

        return json_Decode($result->getBody(), true);
    }
}
